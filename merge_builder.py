#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

from flask import Flask, request
from GitBuildSource import GitBuilder
from shuttle.Shuttle import Shuttle
from builder.git import Git
import os
import time

app = Flask(__name__)
CONF_DIR = "/home/leaeasy/deepin.dsc"
conf_repo = Git(work_dir=CONF_DIR)

T = {}

@app.route('/build', methods=['POST', 'GET'])
def build():
    if request.method == 'GET':
        package = request.args.get('package', None)
        branch = request.args.get('branch', None)
    elif request.method == 'POST':
        package = request.form.get('package', None)
        branch = request.form.get('branch', None)
    else:
        return 'method not support'
    
    if package is None or branch is None:
        return "Error detect!\n package and branch should not to be empty"

    global T
    _time_stamp = time.time()
    if _time_stamp < T.get(package, 0) + 10:
        return "Maybe build too fast, Skip your request..."
    T[package] = _time_stamp
    
    conf_repo.update(branch=branch)
    configfile = os.path.join(CONF_DIR, 'builder.conf')
    if not os.path.exists(configfile):
        return 'builder.conf not exists'

    _p = GitBuilder(configfile, package)

    try:
        _p.prepare_source()
        _p.merge_changelog()
        _p.generate_dsc()
        mirror, cp = _p.upload_source()
        Shuttle().add_job(name=cp['Source'], version=cp['MangledVersion'], priority=cp['Urgency'], dist=cp['Distribution'], mirror=mirror)
    except Exception as e:
        print(e)
    finally:
        _p.clean_workdir()

    return "Build: %s @ %s\n" % (package, branch)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True) 
