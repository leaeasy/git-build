#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

_test = {'dde-daemon': ['go-dlib'], 'dde': ['dde-daemon', 'go-dlib'], 'a': ['dde'], 'b': 'a', 'c': 'a'}


_update = 'go-dlib'

_build = [_update]
r = {_update: 0}

while _build:
    pkg = _build.pop()
    for key in _test:
        if  pkg in _test[key]:
            if key not in _build:
                _build.append(key)
                if r.has_key(key):
                    r[key] = r[pkg] + 1
                else:
                    r[key] = 1

print(r)
