#!/usr/bin/env python
import urllib2
import json
import subprocess
import os
import multiprocessing

GERRIT_URL = 'http://cr.deepin.io'
GITCAFE_URL = 'https://gitcafe.com/Deepin'
WORK_ROOT = '/home/leaeasy/package-git/'

req = urllib2.urlopen(GERRIT_URL+'/projects/')
con = req.read()
_con = con.split('\n',1)[1]

projects = json.loads(_con).keys()

owd = os.getcwd()
try:
    os.chdir(WORK_ROOT)
    cmd_pool=[]
    for p in projects:
        cmd = ['git']
        if os.path.exists(os.path.join(WORK_ROOT,p)):
            cmd.extend(['pull', "%s/%s" % (GITCAFE_URL,p)])
        else:
            cmd.extend(['clone', "%s/%s" % (GITCAFE_URL,p)])
        cmd_pool.append(cmd)

    pool = multiprocessing.Pool(processes=8)
    for cmd in cmd_pool:
        pool.apply_async(subprocess.call,(cmd,))
    pool.close()
    pool.join()
finally:
    os.chdir(owd)
