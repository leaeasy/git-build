#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Shuttle - Deepin packages rebuild tool
#

from shuttle.Shuttle import Shuttle
from shuttle.ShuttleConfig import ShuttleConfig
import sqlobject, sys

# Create database
def create_db():
    try:
        sqlobject.sqlhub.processConnection = sqlobject.connectionForURI(ShuttleConfig().get('build', 'database_uri'))
        from shuttle.Package import Package
        from shuttle.Job import Job
        from shuttle.ShuttleLog import Log
        Package.createTable()
        Job.createTable()
        Log.createTable()
    except Exception as error:
        print("E: %s" % error)
        return 1

    return 0

if len(sys.argv) == 2:
    if sys.argv[1] == "init":
        sys.exit(create_db())
    if sys.argv[1] == "dumpconfig":
        print(ShuttleConfig().dump())
        sys.exit(0)
    if sys.argv[1] == "fix":
        Shuttle().fix_jobs()
        sys.exit(0)
Shuttle().daemon()
