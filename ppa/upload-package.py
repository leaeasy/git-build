#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

from optparse import OptionParser
import os
import sys
from reprepro.repo import Repository

parser = OptionParser()
parser.add_option("--repo-name", dest="repo_name")
parser.add_option("--dist", dest="dist")
parser.add_option("--repo-path", dest="repo_path", default="/srv/pool")
parser.add_option("--incoming-path", dest="incoming_path", default="/srv/pool/incoming/ppa")

(options, args) = parser.parse_args()

incomings = []
for incoming in args:
    _incoming=os.path.join(options.incoming_path, incoming)
    if os.path.exists(os.path.join(options.incoming_path, incoming)):
        incomings.append(_incoming)

repo_path = os.path.join(options.repo_path,'base', options.repo_name)
repo = Repository(repo_path=repo_path)

if options.dist not in repo.get_dists():
    print("initial %s" % options.dist)
    distros =  ['experimental', 'unstable']
    if options.dist not in distros:
        distros.append(options.dist)
    repo.initial(distros=distros, arches=['i386', 'amd64', 'source'], repo_key='debian-mirror@linuxdeepin.com', option_outdir=os.path.join(options.repo_path, 'www', options.repo_name))

if not os.path.exists(os.path.join(repo_path, 'conf/distributions')):
    repo.initial(distros=['experimental', 'unstable'], arches=['i386', 'amd64', 'source'], repo_key='debian-mirror@linuxdeepin.com', option_outdir=os.path.join(options.repo_path, 'www', options.repo_name))

repo.add_package(options.dist, incomings)
