#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import os

from reprepro import conf
from reprepro.helper import LockContext, try_run_command

class Repository():
    def __init__(self, repo_path, name=None):
        self.repo_path = repo_path

        if name is None:
            self.name = os.path.basename(repo_path)
        else:
            self.name = name

    def initial(self, distros, arches, repo_key, skip_incoming=True, option_outdir=None):
        '''
        We may do not need incoming
        '''
        os.makedirs(os.path.join(self.repo_path, 'conf'))

        dist = conf.DistributionsFile(distros=distros, arches=arches, repo_key=repo_key, update_objects=None)
        option = conf.OptionFile(outdir=option_outdir)

        lockfile = os.path.join(self.repo_path, 'lock')
        with LockContext(lockfile) as lock_c:
            distributions_filename = os.path.join(self.repo_path, 'conf/distributions')
            print("Creating distributions file %s" % distributions_filename)
            with open(distributions_filename, 'w') as fp:
                fp.write(dist.generate_file_contents('n/a'))

            if not skip_incoming:
                incoming = conf.IncomingFile(distros=['unstable'])
                incoming_filename = os.path.join(self.repo_path, 'conf/incoming')
                print("Creating incoming file %s" % incoming_filename)
                with open(incoming_filename, 'w') as fp:
                    fp.write(incoming.generate_file_contents())
                incoming.create_required_directories(self.repo_path)

            option_filename = os.path.join(self.repo_path, 'conf/options')
            print("Creating option file %s" % option_filename)
            with open(option_filename, 'w') as fp:
                fp.write(option.generate_file_contents())

    def get_dists(self):
        results = []
        dist_file = os.path.join(self.repo_path, 'conf', 'distributions')
        if not os.path.exists(dist_file):
            return results
        with open(dist_file) as fp:
            for line in fp.readlines():
                if line.startswith('Codename: '):
                    dist=line.strip().split(':')[1].strip()
                    results.append(dist)
        return results

    def get_name(self):
        return self.name

    def _reprepro(self, args):
        lockfile = os.path.join(self.repo_path, 'lock')
        command = [ 'reprepro', '-V',  '--basedir', self.repo_path ]
        command += list(args)
        print(' '.join(command))
        with LockContext(lockfile) as lock_c:
            return try_run_command(command)

    def delete_unreferenced(self):
        command = [ 'deleteunreferenced' ]
        return self._reprepro(_command)
    
    def invalidate_package(self, distro, arch, package):
        """ Remove this package itself from repo"""
        debtype = 'deb' if arch != 'source' else 'dsc'
        arch_match = ',Architecture (== ' + arch + ' )' if arch != 'source' else ''
    
        invalidate_package_command = [ '-T', debtype, '-V', 'removefilter', distro, 
                "package (==" + package + " )" + arch_match ]
    
        return self._reprepro(invalidate_package_command)
    
    def invalidate_dependent(self, distro, arch, package):
        """ Remove all dependencies of the package with the same arch. """
        invalidate_dependent_command = [ '-T', 'deb', 'removefilter', distro, 
                "Architecture (== " + arch + " )," + 
                "( Depends (% *" + package + "[, ]* )" +
                "| Depends (% *" + package + ") )" ]
        return self._reprepro(invalidate_dependent_command)
    
    def update_repo(self, dist_generator, updates_generator, distro, arch, commit=False):
        command_argument = 'update' if command else 'dumpupdate'
        update_command = [ '--noskipold', command_argument, distro ]
    
        lockfile = os.path.join(repo_path, 'lock')
        update_filename = os.path.join(self.repo_path, 'conf/updates')
        distribution_filename = os.path.join(self.repo_path, 'conf/distributions')
    
        with LockContext(lockfile) as lock_c:
            print("Creating updates file %s" % update_filename)
            with open(update_filename, 'w') as fp:
                fp.write(updates_generator.generate_file_contents(distro, arch))
    
            print("Creating distributions file %s" % distribution_filename)
            with open(distribution_filename, 'w') as fp:
                fp.write(dist_generator.generate_file_contents(distro,arch))
    
        print("Running command: %s " % ' '.join(update_command))
        return self._reprepro(update_command)

    def _parse_changes(self, change):
        with open(change) as fp:
            seenfiles = False
            for line in fp.readlines():
                if line.endswith("\n"):
                    line = line[:-1]
                if not seenfiles and line.startswith("Files:"):
                    seenfiles = True
                elif seenfiles:
                    if not line.startswith(' '):
                        break
                    filename = line.split(' ')[-1]
                    yield filename
    
    def add_package(self,  distro, changes):
        for changefile in changes:
            cwd = os.path.dirname(changefile)
            debs = []
            for filename in self._parse_changes(changefile):
                if filename.endswith(".dsc"):
                    _command = [ '--noskipold', '-Pnormal', '--ignore=wrongdistribution', 'includedsc', distro, os.path.join(cwd, filename) ]
                    self._reprepro(_command)

                if filename.endswith(".deb"):
                    debs.append(os.path.join(cwd, filename))

            include_command = ['--noskipold', '-Pnormal', '--ignore=wrongdistribution', 'includedeb', distro]
            include_command.extend(debs)
            self._reprepro(include_command)

    def processincoming(self):
        _command = [ 'processincoming' ]
        return self._reprepro(_command)

    def include_deb(self, distro, debfiles):
        _command = [ '--noskipold', '-Pnormal', 'includedeb', distro ] + list(debfiles)
        return self._reprepro(_command)
    
    def remove_package(self, dist, package):
        _command = [ 'remove', dist, package ]
        return self._reprepro(_command)
