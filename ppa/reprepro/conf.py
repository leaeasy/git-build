#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

import os

ALL_DISTROS = [ 'unstable', 'stable', 'experimental', 'UNRELEASED' ]
ALL_ARCHES = [ 'amd64', 'i386', 'source' ]

class ConfParameters():
    def __init__(self, distros, dest_distros, arches):
        self.distros = distros
        self.arches = arches
        self.dest_distros = dest_distros

class IncomingFile():
    def __init__(self, distros):
        self.distros =distros
        self.standard_snippet = """Name: %(distro)s
IncomingDir: incoming/%(distro)s
TempDir: tmp
Allow: %(distro)s

"""

        self.all_snippet = """Name: all
IncomingDir: incoming/all
TempDir: tmp
Allow: %(distros)s
Cleanup: on_deny on_error
Options: multiple_distributions

"""
        
    def generate_file_contents(self):
        result = ''
        for d in self.distros:
            result += self.standard_snippet % {'distro': d}
        result += self.all_snippet % { 'distros': ' '.join(self.distros) }
        return result

    def create_required_directories(self, repo_path):
        incoming_dirs = [ 'incoming/%s' % d for d in self.distros ]
        incoming_dirs.append('incoming/all')
        for d in incoming_dirs:
            p = os.path.join(repo_path, d)
            if not os.path.isdir(p):
                print("Incoming dir %s did not exists, creating..." % p)
                os.makedirs(p)

class DistributionsFile():
    def __init__(self, distros, arches, repo_key, components=['main'], update_objects=None):
        self.distros = distros
        self.arches = arches
        self.repo_key = repo_key
        self.components = components
        self.update_objects = update_objects

        self.snippet = """Origin: Deepin
Label: Deepin_%(distro)s
Codename: %(distro)s
Suite: %(distro)s
Architectures: %(arches)s
Components: %(components)s
Description: Deepin %(distro)s Repository
SignWith: %(repo_key)s
Update: %(update_rule)s
"""
        
    def generate_file_contents(self, distro):
        result = ''
        for dist in self.distros:
            if self.update_objects:
                update_rule = ' '.join(self.update_objects.get_update_names(distro, dist))
            else:
                update_rule = ''

            d = {'distro': dist,
                 'arches': ' '.join(self.arches),
                 'components': ' '.join(self.components),
                 'repo_key': self.repo_key,
                 'update_rule': update_rule}
        result += self.snippet % d

        return result

class UpdateElement():
    def __init__(self, name, method, suites, component, arches, filter_formula=None):
        self.name = name
        self.method = method
        self.suites  = suites
        self.component = component
        self.arches = arches
        self.filter_formula = filter_formula

    def generate_update_rule(self, distro, arch):
        """
        Name: google-chrome
        Suite: stable
        Architectures: amd64
        Components: main>non-free
        UDebComponents: none
        Method: http://mirror.ufs.ac.za/google-chrome/deb/
        VerifyRelease: blindtrust
        FilterSrcList:upgradeonly google-chrome.list
        """
        if not distro in self.suites:
            return ''
        if not arch in self.arches:
            return ''

        result = ''
        result += 'Name: %s\n' % self.name
        result += 'Method: %s\n' % self.method
        result += 'Suite: %s\n' % distro
        result += 'Components: %s\n' % self.component
        result += 'Architectures: %s\n' % arch

        if self.filter_formula:
            result += 'Filter_formula: %s' % self.filter_formula
        result += '\n'
        return result

class OptionFile():
    def __init__(self, **kwargs):
        self.outdir = kwargs.get('outdir', '+b/www')
        self.logdir = kwargs.get('logdir', '+b/logs')
        self.gnupghome = kwargs.get('gnupghome', None)

    def generate_file_contents(self):
        result =''
        result += 'outdir %s\n' % self.outdir 
        result += 'logdir  %s\n' % self.logdir 
        if self.gnupghome:
            result += 'gnupghome %s\n' % self.gnupghome
        return result
        

class UpdatesFile():
    def __init__(self, distros, arches, repo_key, dest_distros=None):
        self.distros = distros
        self.dest_distros = dest_distros if dest_distros else distros
        self.arches = arches
        self.repo_key = repo_key

        self.update_elements = []

    def generate_file_contents(self, distro, arch):
        result = ''
        for update_element in self.update_elements:
            result += update_element.generate_update_rule(distro, arch)

        return result

    def add_update_element(self, update_element):
        self.update_elements.append(update_element)

    def get_update_name(self, suite, arch):
        update_names = []
        for ele in self.update_elements:
            if suite in ele.suites:
                if arch in ele.arches:
                    update_names.append(ele.name)
        return update_names
