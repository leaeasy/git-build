#!/usr/bin/env python

from shuttle.Package import Package
from shuttle.Shuttle import Shuttle
from shuttle.Job import Job

#Shuttle()

def check_version(name,version, mirror):
    pkg = Package.selectBy(name=name, version=version)
    if pkg.count():
        job = Job.selectBy(package=pkg[0], mirror=mirror).count()
        if job:
            return True
    return False

def get_avaible_version(name, version, mirror):
    _version = version
    i = 0
    flag = True
    while flag:
        flag = check_version(name, version, mirror)
        if flag:
            i += 1
            version = _version+"+b%d" % i
    return version
        
if __name__ == "__main__":
    Shuttle()
    print(get_avaible_version('deepin-file-manager', '0.1.0+r28~g5da3675', 'experimental'))
