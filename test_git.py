#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

from builder.git import Git 

repo="/home/leaeasy/package-git/deepin-feedback/"
branch="master"

g = Git(work_dir=repo)
print("Check out to branch of %s" % branch)
g.checkout(branch)
g.pull()
print("   Git log\n")
print(g.log())
