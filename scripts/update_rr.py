#!/usr/bin/python3
import requests
import re
from optparse import OptionParser

parser = OptionParser()
parser.add_option("--ppa-name", dest="upload_ppa")
(options, args) = parser.parse_args()

upload_ppa = options.upload_ppa
re_ppa = re.compile("http://pools.corp.deepin.com/ppa/(.+)")
headers = {'Access-Token': 'J4wE5fqBE4Fhl5ue6swhx7su1iJB2IEY'}
get_url = "https://rr.deepin.io/api/v1/reviews"

rr_get = requests.get(get_url, headers=headers)
#print(rr_get)
rr_json = rr_get.json()
rr_reviews = rr_json['result']['reviews']
reviews_info = []
for reviews in rr_reviews:
    if reviews['status'] == 'open':
        if reviews['abandoned'] == 0:
            if reviews['ppa_codename'] == 'unstable':
                ppa_url = reviews['ppa']
                ppa_name = re_ppa.findall(ppa_url)[0]
                review_info = {}
                review_info['ppa_name'] = ppa_name
                review_info['id'] = reviews['id']
                reviews_info.append(review_info)

print(reviews_info)
for review_ppa in reviews_info:
    if review_ppa['ppa_name'] == upload_ppa:
        print(review_ppa['id'])
        patch_url = "https://rr.deepin.io/api/v1/review/:%s" % review_ppa['id']
        print('upload ppa in rr')
        print('id is %s' % review_ppa['id'])
        review_update_headers = {'Access-Token': 'J4wE5fqBE4Fhl5ue6swhx7su1iJB2IEY', 'retrigger': 1}
        rr_patch = requests.patch(patch_url, review_update_headers)
