#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
from builder.exc import NoConfigError
from builder.dch import Dch, mangle_changelog
from builder.tag import Tag
import os
from builder.config import PerConfig, JobConfig
import tempfile
from builder.git import Git
import shutil
from builder.source import generate_dsc
import subprocess
from shuttle.Shuttle import Shuttle
from shuttle.Package import Package
from shuttle.Job import Job

class UploadSourceError(Exception):
    pass

class GitBuilder():
    def __init__(self, config_file, package_name, debian_branch=None, upstreamer_branch=None):
        if not os.path.exists(config_file):
            raise NoConfigError("No Configfile for %s" % config_file)

        self.perconfig = PerConfig(config_file)
        self.notify = self.perconfig.fetch_value('build','notify')
        
        self.upstreamer_basedir = self.perconfig.fetch_value('build','upstreamer_basedir')
        if self.upstreamer_basedir is None:
            raise NoConfigError("No upstreamer_basedir value")

        self.debian_basedir = self.perconfig.fetch_value('build','debian_basedir')
        if self.debian_basedir is None:
            self.debian_basedir = self.upstreamer_basedir

        if debian_branch and debian_branch != "master":
            self.debian_branch = "origin/"+ debian_branch
        else:
            self.debian_branch = "master"
        Git(work_dir=self.debian_basedir).update(branch=self.debian_branch)
        self.perconfig.reload()

        job_config_file = self.perconfig.fetch_value('build','job_config')
        self.fallback_version = None
        self.trigger_packages = None
        if job_config_file and os.path.exists(os.path.join(self.debian_basedir, job_config_file)):
            job_config = JobConfig(os.path.join(self.debian_basedir, job_config_file))
            pkg_info = job_config.fetch_info(item=package_name)
            _upstreamer_branch = job_config.fetch_value(item='default', name="default_branch")

            self.base_version = pkg_info.get('base_version') or '0.0'
            self.package_name = pkg_info.get('pkg') or package_name
            if pkg_info.get('trigger'):
                self.trigger_packages =[i.strip() for i in  pkg_info.get('trigger').split(',') ]
            self.upstreamer_branch = upstreamer_branch or pkg_info.get("branch")
            self.fallback_version = pkg_info.get('version')
            self.package_host = pkg_info.get('package_host') or job_config.fetch_value(item='default', name='package_host')

            self.dist = job_config.fetch_value(name='dist') or self.perconfig.fetch_value('build','dist', 'unstable')
        else:
            raise NoConfigError("No configfile %s" % job_config_file)


        if self.upstreamer_branch is None:
            self.upstreamer_branch = _upstreamer_branch or "master"


        # TODO use remoting  branch default
        self.upstreamer_branch = "origin/"+self.upstreamer_branch
        self._upstreamer_branch = self.upstreamer_branch

        self._source_repo = os.path.join(self.upstreamer_basedir, self.package_name)
        if os.path.exists(os.path.join(self._source_repo, 'debian')):
            self._debian_repo = None
        else:
            self._debian_repo = os.path.join(self.debian_basedir, self.package_name)

        git_url = pkg_info.get('git')
        if git_url is None:
            try:
                git_url = os.path.join(job_config.fetch_value(item='default', name='git_base'), package_name)
            except Exception as error:
                print(error)
                raise NoConfigError("no git base url found")

        Git(work_dir=self._source_repo, git_url=git_url).update(branch=self.upstreamer_branch)


        self._dch=Dch(upstreamer_repo=self._source_repo, debian_repo=self._debian_repo, upstreamer_branch=self.upstreamer_branch, debian_branch=self.debian_branch)

        self._tag = None
        self._tag_version = self.perconfig.fetch_value('build','tag_version','1')
        if self._tag_version == "1":
            self._tag=Tag(self._source_repo)
        self.cp = {}

    def prepare_source(self):
        def check_version(name, version, mirror):
            print("Check Version ==> Name: %s  Version: %s Mirror: %s" % (name, version, mirror))
            pkg = Package.selectBy(name=name, version=version)
            if pkg.count():
                job = Job.selectBy(package=pkg[0], mirror=mirror).count()
                print(job)
                if job:
                    return True
            return False

        def get_avaible_version(name, version, mirror):
            _version = version
            i = 0
            flag = True
            while flag:
                flag = check_version(name, version, mirror)
                if flag:
                    i += 1
                    version = _version + ".b%d" %i
            return version

        self.cp['Source'] = self.package_name
        if self._tag:
            try:
                #init shuttle
                Shuttle()
                _version = self._tag.pkgver(branch=self._upstreamer_branch, fallback_version=self.fallback_version)
                if self.package_host:
                    version = get_avaible_version(self.package_name, _version, self.package_host)
                else:
                    version = get_avaible_version(self.package_name, _version, self.dist)
            except Exception as e:
                print(e)
                raise OSError("No tag found!")
        else:
            version = (self.base_version + '+' + str(self._dch.mangle_verison()))

        self.cp['MangledVersion'] = version
        self.cp['Distribution'] = self.dist
        self.cp['Urgency'] = 'low'

        temp_dir = os.path.join("/tmp","%s-temp" % self.dist)
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)
        self._work_dir = tempfile.mkdtemp(dir=temp_dir, prefix="%s-" % self.package_name)

        
        def copy_upstream_source(snapshot=None):
            g = Git(work_dir=self._source_repo)
            print("Check out to branch of %s" % self.upstreamer_branch)
            g.fetch()
            if snapshot is None:
                snapshot=self._dch.revision()
            g.checkout(self.upstreamer_branch)
            dest_dir = os.path.join(self._work_dir, self.cp['Source']+'-'+self.cp['MangledVersion'])
            print("Copy Upstreamer source: %s -> %s" %(self._source_repo, dest_dir))
            prefix=self.cp['Source']+'-'+self.cp['MangledVersion']
            if not prefix.endswith('/'):
                prefix += '/'
            dest_dir = os.path.join(self._work_dir, prefix).encode('utf-8')
            try:
                os.system("mkdir -p %s" % dest_dir)
                with open(os.devnull, 'w') as devnull:
                    subprocess.check_call("git archive --format=tar --prefix=%s %s | (cd %s && tar xvf -)" % (prefix, snapshot, self._work_dir), shell=True, cwd=self._source_repo, stdout=devnull, stderr=devnull)
            except Exception as e:
                print(self._source_repo, dest_dir)
                raise e
        
            garbage_clean(dest_dir)
            return dest_dir
           
        def garbage_clean(path):
            exclude_dir = ['.git','.bzr','.svn' ]
            for d in exclude_dir:
                if os.path.exists(os.path.join(path,d)):
                    print("clean %s dir" % d)
                    shutil.rmtree(os.path.join(path,d))
            #fix timestamp error
            subprocess.check_call("find %s -exec touch \{\} \;" % path, shell=True)

        def debianize(path, format="quilt"):
            if not os.path.exists(path):
                os.makedirs(path)
            _format = os.path.join(path, 'source', 'format')
            if os.path.exists(_format):
                with open(_format, 'w') as fp:
                    fp.writelines("3.0 (%s)\n" % format)

        
        def copy_source():
            _source_root = copy_upstream_source()
            print("Check out to branch of %s" % self.debian_branch)
            if not os.path.exists(os.path.join(_source_root, 'debian')): 
                print("Copy Debian folder: %s -> %s" %(os.path.join(self._debian_repo,'debian'),os.path.join( _source_root,'debian')))
                try:
                    Git(work_dir=self._debian_repo).checkout(self.debian_branch)
                except Exception as e:
                    print(e)
                    pass
                shutil.copytree(os.path.join(self._debian_repo,'debian'), os.path.join(_source_root,'debian'))
            debianize(os.path.join(_source_root, 'debian'), format="native")
            return _source_root
        
        self._source_root = copy_source()
        
    def merge_changelog(self):
        snapshot=self._dch.revision()
        content=self._dch.changelog()
        mangle_changelog(os.path.join(self._source_root, "debian", "changelog"), self.cp, self.upstreamer_branch, content, snapshot)

    def generate_dsc(self):
        self.sources=generate_dsc(self._source_root)

    def upload_source(self):
        source_dir, source_files = self.sources
        upload_method = self.perconfig.fetch_value("source","method")
        upload_dest_base = self.perconfig.fetch_value("source","base_dir")
        if self.package_host is None:
            package_host = self.perconfig.fetch_value("repo","host")
            upload_dest = os.path.join(upload_dest_base, self.perconfig.fetch_value("source","dest"))
        else:
            package_host = self.package_host
            upload_dest = os.path.join(upload_dest_base, "ppa")

        if upload_method is None:
            raise UploadSourceError("Upload cmd error: upload_method is None")

        if upload_dest is None:
            raise UploadSourceError("Upload cmd error: upload_dest is None")

        for f in source_files:
            f_path = os.path.join(source_dir, f)
            upload_cmd = [ upload_method, f_path, upload_dest ]
            status = subprocess.call(upload_cmd)
            if status !=0 :
                raise UploadSourceError("Upload  %s: %s to %s failed" % ( upload_method, f, upload_dest))
                break
        return package_host, self.cp

    def clean_workdir(self):
        import glob
        try:
            shutil.rmtree(self._work_dir)
            for f in glob.glob(os.path.join(self._work_dir,self.cp['Source']+'_'+self.cp['MangledVersion']+"*")):
                os.unlink(f)
        except:
            pass

    def notify_message(self, message):
        color = {"warn":"yellow", "error":"red", "normal":"green", "notice":"purple", "default":"gray"}
        if self.notify == "1":
            try:
                import hipchat
            except ImportError:
                return 
            token = self.perconfig.fetch_value("hipchat","token")
            room_id = self.perconfig.fetch_value("hipchat","room_id")
            if token is not None and room_id is not None:
                message_color = color.get(message.get("type","default"), "gray")
                hipster = hipchat.HipChat(token = token)
                hipster.message_room(room_id, 'GitBuilder', message['content'], color=message_color)


if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-c","--config", dest="configfile", help="Configuration file to parse.")
    parser.add_option("-p","--package", dest="packagename", help="Specify a packagename")
    parser.add_option("-b","--branch", dest="branch", help="Specify a branch of debian")
    (options, args) = parser.parse_args()
    if options.configfile != None:
        if not os.path.exists(options.configfile):
            parser.error("option -c or --config not exists")
    else:
        parser.error("option -c or --config must be exists")
    if options.branch is None:
        options.branch = "debian"
    a=GitBuilder(options.configfile, options.packagename, options.branch)
    trigger_packages = a.trigger_packages
    try:
        print("Step 1: Prepare Source...")
        a.prepare_source()
        print("Step 2: Merge changelog...")
        a.merge_changelog()
        print("Step 3: Generate Dsc file...")
        a.generate_dsc()
        print("Step 4: Upload Sources...")
        mirror,cp=a.upload_source()
        print("  * Upload %s:%s to mirror:%s " % (cp['Source'], cp['MangledVersion'], mirror))
        job_id = Shuttle().add_job(name=cp['Source'], version=cp['MangledVersion'], priority=cp['Urgency'], dist=cp['Distribution'], mirror=mirror)
    except Exception as e:
        print(e)
    finally:
        a.clean_workdir()

    if trigger_packages:
        print("Step 5: Trigger packages...")
        for package in trigger_packages:
            p = GitBuilder(options.configfile, package, options.branch)
            try:
                p.prepare_source()
                p.merge_changelog()
                p.generate_dsc()
                mirror,cp=p.upload_source()
                if cp:
                    _job_id = Shuttle().add_job(name=cp['Source'], version=cp['MangledVersion'], priority=cp['Urgency'], dist=cp['Distribution'], mirror=mirror)
                    Shuttle().add_deps(_job_id, [job_id])
            finally:
                p.clean_workdir()
