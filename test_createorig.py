#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

DEBIAN_REPO="/home/leaeasy/Downloads/dh-python-1.20131003"
DEBIAN_BRANCH="master"

UPSTREAM_REPO="/home/leaeasy/Downloads/dh-python-1.20131003"
UPSTREAM_BRANCH="master"

WORKDIR="/tmp/debuild"

import os
import shutil
from git import Git 
def prepare_workdir():
    if os.path.exists(WORKDIR):
       shutil.rmtree(WORKDIR) 
    print("Create Workdir: %s" % WORKDIR)
    os.makedirs(WORKDIR)

def copy_upstream_source(repo, branch):
    g = Git(work_dir=repo)
    print("Check out to branch of %s" % branch)
    g.checkout(branch)
    print("   Git log\n")
    print(g.log())
    dest_dir = os.path.join(WORKDIR, os.path.basename(repo))
    print("Copy Upstreamer source: %s -> %s" %(repo, dest_dir))
    try:
        shutil.copytree(repo, dest_dir)
    except:
        pass

    garbage_clean(dest_dir)
    return dest_dir
   
def garbage_clean(source_path):
    if os.path.exists(os.path.join(source_path,'.git')):
        print("clean .git dir")
        shutil.rmtree(os.path.join(source_path,'.git'))
    if os.path.exists(os.path.join(source_path,'debian')):
        print("clean debian dir")
        shutil.rmtree(os.path.join(source_path,'debian'))

def copy_source(upstream_repo, upstream_branch, debian_repo, debian_branch):
    _dest_dir = copy_upstream_source(upstream_repo, upstream_branch)
    print("Check out to branch of %s" % debian_branch)
    print("Copy Debian folder: %s -> %s" %(os.path.join(debian_repo,'debian'),os.path.join( _dest_dir,'debian')))
    Git(work_dir=debian_repo).checkout(debian_branch)
    shutil.copytree(os.path.join(debian_repo,'debian'), os.path.join(_dest_dir,'debian'))

prepare_workdir()
copy_source(UPSTREAM_REPO, UPSTREAM_BRANCH, DEBIAN_REPO, DEBIAN_BRANCH)
