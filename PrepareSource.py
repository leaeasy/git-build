#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
from builder.exc import NoConfigError
from builder.dch import Dch, mangle_changelog
import os
from builder.config import PerConfig
import tempfile
from builder.git import Git
import shutil
from builder.source import generate_dsc
import subprocess
from shuttle.Shuttle import Shuttle

class UploadSourceError(Exception):
    pass

class GitBuilder():
    def __init__(self, config_file, package_name,  upstreamer_branch=None, debian_repo=None, debian_branch=None):
        if not os.path.exists(config_file):
            raise NoConfigError("No Configfile for %s" % config_file)

        self.perconfig = PerConfig(config_file)
        self.dest = self.perconfig.repo

        self.notify = self.perconfig.fetch_value('build','notify')
        
        self.upstreamer_basedir = self.perconfig.fetch_value('build','upstreamer_basedir')
        _base_version = self.perconfig.fetch_value(package_name, 'base_version')
        if _base_version is None:
            _base_version = self.perconfig.fetch_value('build', 'base_version','0.0.0')
        self.base_version = _base_version
        if self.upstreamer_basedir is None:
            raise NoConfigError("No upstreamer_basedir value")

        self.debian_basedir = self.perconfig.fetch_value('build','debian_basedir')
        if self.debian_basedir is None:
            self.debian_basedir = self.upstreamer_basedir

	self.package_name = self.perconfig.fetch_value(package_name, 'packagename') or package_name

	_upstreamer_branch = self.perconfig.fetch_value('build', 'default_branch', None)
        self.upstreamer_branch = upstreamer_branch if upstreamer_branch else self.perconfig.fetch_value(package_name, "upstreamer_branch", None)
        if self.upstreamer_branch is None:
            self.upstreamer_branch = _upstreamer_branch or "master"

        self.debian_repo = debian_repo if debian_repo  else self.perconfig.fetch_value(package_name, "debian_repo", self.package_name)

	_debian_branch = self.perconfig.fetch_value('build', 'debian_branch', 'master')
        self.debian_branch = debian_branch if debian_branch else self.perconfig.fetch_value(package_name, "debian_branch",None)
	if self.debian_branch is None:
            self.debian_branch = _debian_branch

        self._source_repo = os.path.join(self.upstreamer_basedir, self.package_name)
        self._debian_repo = os.path.join(self.debian_basedir, self.debian_repo)
        self._dch=Dch(upstreamer_repo=self._source_repo, debian_repo=self._debian_repo, upstreamer_branch=self.upstreamer_branch, debian_branch=self.debian_branch)
        Git(work_dir=self._source_repo).update(branch=self.upstreamer_branch)


    def prepare_source(self):
        self.cp = {}
        self.cp['Source'] = self.package_name
        self.cp['MangledVersion'] =(self.base_version + '+' + str(self._dch.mangle_verison()))
        self.cp['Distribution'] = self.perconfig.fetch_value('build','dist','trusty')
        self.cp['Urgency'] = 'low'

        temp_dir = os.path.join("/tmp","%s-temp" % self.dest)
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)
        self._work_dir = tempfile.mkdtemp(dir=temp_dir, prefix="%s-" % self.package_name)
        
        def copy_upstream_source():
            g = Git(work_dir=self._source_repo)
            #g.pull()
            print("Check out to branch of %s" % self.upstreamer_branch)
            g.checkout(self.upstreamer_branch)
            g.pull()
            dest_dir = os.path.join(self._work_dir, self.cp['Source']+'-'+self.cp['MangledVersion'])
            print("Copy Upstreamer source: %s -> %s" %(self._source_repo, dest_dir))
            try:
                shutil.copytree(self._source_repo, dest_dir)
            except:
                pass
        
            garbage_clean(dest_dir)
            return dest_dir
           
        def garbage_clean(path):
            exclude_dir = ['.git','.bzr','.svn','debian']
            for d in exclude_dir:
                if os.path.exists(os.path.join(path,d)):
                    print("clean %s dir" % d)
                    shutil.rmtree(os.path.join(path,d))
        
        def copy_source():
            _source_root = copy_upstream_source()
            print("Check out to branch of %s" % self.debian_branch)
            print("Copy Debian folder: %s -> %s" %(os.path.join(self._debian_repo,'debian'),os.path.join( _source_root,'debian')))
            try:
                Git(work_dir=self._debian_repo).checkout(self.debian_branch)
            except Exception as e:
                print(e)
                pass
            shutil.copytree(os.path.join(self._debian_repo,'debian'), os.path.join(_source_root,'debian'))
            return _source_root
        
        self._source_root = copy_source()
        
    def merge_changelog(self):
        snapshot=self._dch.revision()
        content=self._dch.changelog()
        mangle_changelog(os.path.join(self._source_root, "debian", "changelog"), self.cp, self.upstreamer_branch, content, snapshot)

    def generate_dsc(self):
        self.sources=generate_dsc(self._source_root)

    def upload_source(self):
        source_dir, source_files = self.sources
        upload_method = self.perconfig.fetch_value("source","method")
        upload_dest = self.perconfig.fetch_value("source","dest")
        package_host = self.perconfig.fetch_value("package","host")
        if upload_method is None:
            raise UploadSourceError("Upload cmd error: upload_method is None")
        if upload_dest is None:
            raise UploadSourceError("Upload cmd error: upload_dest is None")
        for f in source_files:
            f_path = os.path.join(source_dir, f)
            upload_cmd = [ upload_method, f_path, upload_dest ]
            status = subprocess.call(upload_cmd)
            if status !=0 :
                raise UploadSourceError("Upload  %s: %s to %s failed" % ( upload_method, f, upload_dest))
                break
        return package_host, self.cp

    def clean_workdir(self):
        import glob
        try:
            shutil.rmtree(self._work_dir)
            for f in glob.glob(os.path.join(self._work_dir,self.cp['Source']+'_'+self.cp['MangledVersion']+"*")):
                os.unlink(f)
        except:
            pass

    def notify_message(self, message):
        notify = self.perconfig.fetch_value('bearychat','notify')
        if notify == "1":
            try:
                from shuttle.plugin import bearychat
            except ImportError:
                return 
            url = self.perconfig.fetch_value("bearychat","url")
            if url is not None:
                bc = bearychat.BearyChat(url=url)
                bc.message(message)


if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-c","--config", dest="configfile", help="Configuration file to parse.")
    parser.add_option("-p","--package", dest="packagename", help="Specify a packagename")
    (options, args) = parser.parse_args()
    print options.configfile
    if options.configfile != None:
        if not os.path.exists(options.configfile):
            parser.error("option -c or --config not exists")
    else:
        parser.error("option -c or --config must be exists")
    a=GitBuilder(options.configfile, options.packagename)
    try:
        print("Step 1")
        a.prepare_source()
        print("Step 2")
        a.merge_changelog()
        print("Step 3")
        a.generate_dsc()
        mirror,cp=a.upload_source()
    except Exception as e:
        print(e)
    finally:
        a.clean_workdir()
