#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

from shuttle.ShuttleHTTPServer import ShuttleHTTPServer
from shuttle.ShuttleConfig import ShuttleConfig
import sys, os

try:
    os.chdir(ShuttleConfig().get('http','templates_dir'))
except Exception as error:
    print("E: cannot chdir to templates_dir: %s" % error)
    sys.exit(1)

ShuttleHTTPServer().start()
