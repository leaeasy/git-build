from builder.tag import Archiver
from shuttle.Shuttle import Shuttle
import traceback

archiver=Archiver(config="/home/leaeasy/deepin.dsc.bak/git-build.conf")
build_dist="unstable"

if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-p", "--package", dest="packagename", help="Spectify a packagename")
    parser.add_option("-f", "--force", action="store_true", dest="force", default=False, help="force build")
    parser.add_option("-r", "--rebuild", action="store_true", dest="rebuild", default=False, help="rebuild build")
    parser.add_option("-t", "--tag", dest="tagver", help="Spectify a tag version")
    (options, args) = parser.parse_args()
    force = options.force
    info = archiver.archive(options.packagename, tagver=options.tagver, force=force)
    try:
        if not force:
            if int(info['tagver'].split('.')[0]) > 100:
                raise OSError("version too big:%s" % info['tagver'])
        version = archiver.debianize(info, rebuild=options.rebuild)
        if version:
            Shuttle().add_job(name=info['source'], version=version, priority='low', dist=build_dist, mirror=info['mirror'])
    except Exception as e:
        traceback.print_exc()
        print("Error detect %s: %s" % (options.packagename, e))
