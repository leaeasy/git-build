#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
# manage build jobs 

from shuttle.Shuttle import Shuttle
from shuttle.Package import Package
from shuttle.JobStatus import JobStatus
from shuttle.Job import Job
import sys, os
from termcolor import cprint

def usage():
    print("%s -- Manage job for Shuttle" % os.path.basename(sys.argv[0]))
    print("Usage: %s [command] <opts>\n" % os.path.basename(sys.argv[0]))
    print("Commands:")
    print("   add                     - add jobs, reading from stdin")
    print("   add-deps                - add build-depends, reading from stdin")
    print("   add-quinn-diff <dist>   - add package reading quinn-diff input from stdin")
    print("   requeue <jobid>         - requeue job for shuttle")
    print("   delete <jobid>          - delete job")
    print("   force-delete <jobid>    - force delete job")
    print("   list-deps               - list the whole depends table")
    print("   list <criteria>=<value> - list jobs matching criteria")
    print("   release-jobs            - release locked jobs")
    print("   stats                   - view a nice graph of build status")
    sys.exit(1)

    sys.exit(1)

def add_deps():
    empty = True
    for line in sys.stdin.readlines():
        empty = False
        args = line.strip().split(' ')
        Shuttle().add_deps(job_id=args[0], dependency_ids=args[1:])
    if empty:
        print("E: usage: %s\n read <job_id> <dependency_job_id> [dependency_job_id] [...] from stdin" % sys.argv[0])
        sys.exit(1)

def dep_list_to_string(job_lists):
    output_string = ""
    for job in job_lists:
        output_string = output_string + " " + str(job.id)
    return output_string

def display_dep():
    jobs = Job.selectBy()
    print("  id  |    dependencies     ")
    print("------+----------------------")
    for job in jobs:
        print("%04.4s | %021.21s" % (job.id, dep_list_to_string(job.deps)))

def add():
    empty = True
    for line in sys.stdin.readlines():
        empty = False
        args = line.strip().split(' ')
        if len(args) == 4:
            Shuttle().add_job(args[0], args[1], args[2], args[3])
        if len(args) == 5:
            Shuttle().add_job(args[0], args[1], args[2], args[3], mailto=None, mirror=None, arch=args[4])
        if len(args) == 6:
            Shuttle().add_job(args[0], args[1], args[2], args[3], mirror=args[5], mailto=None, arch=args[4])
        if len(args) == 7:
            Shuttle().add_job(args[0], args[1], args[2], args[3], mirror=args[5], mailto=args[6], arch=args[4])
    if empty:
        print("E: usage: %s\n   read <package> <version> <priority> <dist> [arch] [mirror] [mailto] from stdin" % sys.argv[0])
        sys.exit(1)

def add_quinn_diff(dist):
    for line in sys.stdin.readlines():
        name = line.split('/')[1].split('_')[0]
        version = line.split('/')[1].split('_')[1].split('.dsc')[0]
        priority = line.split('/')[1].split('_')[1].split('.dsc')[1].split(':')[0][2:]
        Shuttle().add_job(name, version, priority, dist)

def requeue_job(jobid):
    if not Shuttle().requeue_job(jobid):
        print("E: rebuild not accepted")
        sys.exit(1)

def release_jobs():
    if not Shuttle().release_jobs():
        print("E: rebuild release error")
	sys.exit(1)

def list():
    if len(sys.argv) == 3:
        try:
            (critname, critvalue) = sys.argv[2].split('=')
        except ValueError:
            print("E: usage: %s list criteria=value" % os.path.basename(sys.argv[0]))
            return False
    else:
        print_jobs(Job.selectBy())
        return True

    if critname == "package":
        critvaluepkg = critvalue.split('_')
        pkgname = critvaluepkg[0]
        pkgver = None
        if len(critvaluepkg) > 1:
            pkgver = critvaluepkg[1]
        if pkgver:
            pkgs = Package.selectBy(name=pkgname, version=pkgver)
        else:
            pkgs = Package.selectBy(name=pkgname)
        for pkg in pkgs:
            print_jobs(Job.selectBy(package=pkg))

    if critname == "arch":
        print_jobs(Job.selectBy(arch=critvalue))

    if critname == "dist":
        print_jobs(Job.selectBy(dist=critvalue))

    if critname == "host":
        print_jobs(Job.selectBy(host=critvalue))

    if critname == "mirror":
        print_jobs(Job.selectBy(mirror=critname))

    if critname == "status":
        try:
            print_jobs(Job.selectBy(status=getattr(JobStatus, critvalue)))
        except AttributeError:
            print("E: unknown status")
            sys.exit(1)

    if critname == "id":
        print_jobs(Job.selectBy(id=critvalue))

def stats():
    nbjobs = Job.selectBy().count()
    wait = Job.selectBy(status=JobStatus.WAIT).count()
    wait += Job.selectBy(status=JobStatus.WAIT_LOCKED).count()
    building = Job.selectBy(status=JobStatus.BUILDING).count()
    ok = Job.selectBy(status=JobStatus.BUILD_OK).count()
    failed = Job.selectBy(status=JobStatus.SOURCE_FAILED).count()
    failed += Job.selectBy(status=JobStatus.BUILD_FAILED).count()
    failed += Job.selectBy(status=JobStatus.POST_BUILD_FAILED).count()

    if nbjobs > 0:
        wait *= 10.0 / nbjobs
        building *= 10.0 / nbjobs
        failed *= 10.0 / nbjobs
        ok *= 10.0 / nbjobs
    else:
        wait = 0.0
        building = 0.0
        failed = 0.0
        ok = 0.0

    lines = 10
    while lines > 0:
        cprint("%3.0d %% |" % (lines * 10), 'yellow', end='')
        if int(round(wait)) >= lines:
            cprint("  ##  ", 'white', end='')
        else:
            print("      "),
        if int(round(building)) >= lines:
            cprint(" ##  ", 'blue', end='')
        else:
            print("    "),
        if int(round(failed)) >= lines:
            cprint(" ##  ", 'red', end='')
        else:
            print("    "),
        if int(round(ok)) >= lines:
            cprint(" ##  ", 'green', end='')
        else:
            print("    "),
        lines -= 1
        print("")
    cprint("      +----------------------",'yellow')
    cprint("         WA   BU   FA   OK", "green")

def delete(jobid, force=False):
    job = Job.get(jobid)
    if force is True:
        job.destroySelf()
        print("I: job %s force deleted" % jobid)
    elif job.status != JobStatus.BUILDING:
        job.destroySelf()
        print("I: job %s deleted" % jobid)
    else:
        print("E: can't delete job, build status is %s and should not be BUILDING" % JobStatus.whatis(job.status))

def print_headers():
    print("  id  |     package name     |    version     |    status    |  host  | dist  |  arch  | mirror")
    print("------+----------------------+----------------+--------------+--------+-------+--------+-------")

def print_jobs(jobs):
    try:
        print_headers()
        for job in jobs:
            print("%05.5s | %020.20s | %014.14s | %012.12s | %06.6s | %05.5s | %06.6s | %06.6s"% \
                    (job.id, job.package.name, job.package.version,
                     JobStatus.whatis(job.status),
                     job.host, job.dist, job.arch, job.mirror))
    except IOError:
        pass

if len(sys.argv) > 1:
    # Init system
    Shuttle()
    if sys.argv[1] == "add":
        add()
    if sys.argv[1] == "add-deps":
        add_deps()
    if sys.argv[1] == "list-deps":
        display_dep()
    if sys.argv[1] == "add-quinn-diff" and len(sys.argv) > 2:
        add_quinn_diff(sys.argv[2])
    if sys.argv[1] == "list":
        list()
    if sys.argv[1] == "stats":
        stats()
    if sys.argv[1] == "release-jobs":
        release_jobs()
    if sys.argv[1] == "delete" and len(sys.argv) > 2:
        delete(int(sys.argv[2]))
    if sys.argv[1] == "force-delete" and len(sys.argv) > 2:
        delete(int(sys.argv[2]), force=True)
    if sys.argv[1] == "requeue" and len(sys.argv) == 3:
        requeue_job(int(sys.argv[2]))
else:
    usage()
