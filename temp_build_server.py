#! /usr/bin/env python

from bottle import route,run
from GitBuildSource import GitBuilder
from shuttle.Shuttle import Shuttle

@route("/build/:name")
def build(name):
    a = GitBuilder('conf/unstable.conf', name)
    try:
        a.prepare_source()
        a.merge_changelog()
        a.generate_dsc()
        mirror, cp = a.upload_source()
        Shuttle().add_job(name=cp['Source'], version=cp['MangledVersion'], priority=cp['Urgency'], dist=cp['Distribution'], mirror=mirror)
        message = {"type": "success", "content":"Successfully %s build source" % name}
    except Exception as e:
        message = {"type":"error","content":"Prepare %s Failed. Error: %s" % (name,e)}
    finally:
        a.clean_workdir()
    return message

if __name__ == "__main__":
    run(host="0.0.0.0")
