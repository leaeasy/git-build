#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
import os
import time
from builder.git import Git

def guess_builder_email(default=None):
    return os.environ.get("DEBEMAIL",default)

def guess_builder_name(default=None):
   return os.environ.get("DEBFULLNAME",default)  

def get_changelog_time():
    return time.strftime("%a, %d %b %Y %T %z", time.localtime())

def mangle_changelog(changelog, cp, branch, content, snapshot, merge=True, debug=True):
    try:
        tmpfile = '%s.%s' % (changelog, "dch")
        cw = open(tmpfile, 'w')
        cw.write("%(Source)s (%(MangledVersion)s) %(Distribution)s; urgency=%(Urgency)s\n" % cp) 
        cw.write("\n")
        cw.write("  ** Build revision: %s  @  %s **\n\n" % (snapshot, branch))
        if debug:
            print("%(Source)s (%(MangledVersion)s) %(Distribution)s; urgency=%(Urgency)s\n" % cp) 
            print("  ** Build revision: %s  @  %s **\n\n" % (snapshot, branch))

        if isinstance(content, str):
            cw.write("  * %s\n" % content)
            cw.write("\n")
        if isinstance(content, tuple):
            for i in content:
                cw.write("  * %s\n" % i)
            cw.write("\n")
        if isinstance(content, dict):
            for i in content:
                cw.write("  [%s]\n" % i)
                for j in content[i]:
                    cw.write("  * %s\n" % j)
                cw.write("\n")
        cw.write(" -- %s <%s>  %s\n" % (guess_builder_name(), guess_builder_email(), get_changelog_time()))
        cw.write("\n")
        if merge:
            with open(changelog) as fp:
                for line in  fp.readlines():
                    cw.write(line)
        cw.close()

        os.rename(tmpfile, changelog)
    except Exception as e: 
        print(e)
        pass

class Dch(object):
    def __init__(self, upstreamer_repo,upstreamer_branch="master",debian_repo=None, debian_branch="master", update=True):
        self.upstreamer_repo=upstreamer_repo
        self.upstreamer_branch = upstreamer_branch

        self.debian_repo=debian_repo
        self.debian_branch=debian_branch

        self.upstreamer=Git(self.upstreamer_repo)
        if debian_repo is not None:
            self.debian=Git(self.debian_repo)
        else:
            self.debian=self.upstreamer


    def changelog(self):
        result={}
        #if self.debian_repo is not None:
        #    result['debian']=self.debian.log(self.debian_branch,maximum=1)
        result['upstreamer']=self.upstreamer.log(self.upstreamer_branch, maximum=1)

        return result
    
    def buildtime(self):
        return time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))

    def revision(self,length=None):
        _rev = self.upstreamer.revision(branch=self.upstreamer_branch)
        print("Fetch revision is %s" % _rev)
        if length is not None:
            return _rev[0:length]
        else:
            return _rev

    def mangle_verison(self):
        return "git" + self.buildtime() + "~" + self.revision(length=10)

    def __str__(self):
        result = ""
        result += "git" + self.buildtime() + "~" + self.revision() +"\n\n"
        changelog = self.changelog()
        for section in changelog:
            result += "[" + section + "]\n"
            for i in changelog[section]:
                result += "%s\n" % i
            result += "\n"
        return result

