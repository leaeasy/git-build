#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

class GitCommandError(Exception):
    def __init__(self, command, status, stderr=None):
        self.stderr = stderr
        self.status = status
        self.command = command

    def __str__(self):
        return("'%s' returned exit status %i: %s" % (' '.join(str(i) for i in self.command), self.status, self.stderr))

class NoDestError(Exception):
    pass

class NoConfigError(Exception):
    pass
