#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import os
from subprocess import Popen,PIPE
from builder.exc import GitCommandError

class Git(object):

    def __init__(self, work_dir=None, git_url=None):
        if os.getuid() == 0:
            raise OSError("Cannot run as root")
        self._work_dir = work_dir
        if not os.path.exists(self._work_dir):
            print("Warning: %s not exists" % self._work_dir)
            self.init(git_url=git_url)

    def init(self,git_url=None):
        if git_url is not None:
            Popen(['git', 'clone', git_url, os.path.basename(self._work_dir)], cwd=os.path.dirname(self._work_dir)).wait()
        else:
            raise OSError("No git_url: %s detect!" % git_url)


    @property
    def work_dir(self):
        return self._work_dir

    def execute(self, subprocess_args=None, extended_output=None):
        if self._work_dir is None:
            cwd = os.getcwd()
        else:
            cwd = self._work_dir
	
            
        env = os.environ.copy()
        env["LC_MESSAGE"]="C"
        execute_cmd = ['git']+list(subprocess_args)
        print("execute: %s @ %s" % (" ".join(execute_cmd),cwd))
        proc = Popen(execute_cmd, env=env, cwd=cwd, stderr=PIPE,stdout=PIPE)
        try:
            stdout,stderr = proc.communicate()
            status = proc.returncode
            if stdout.endswith("\n"):
                stdout = stdout[:-1]
            if stderr.endswith("\n"):
                stderr = stdout[:-1]
        finally:
            proc.stdout.close()
            proc.stderr.close()

        if status != 0:
            raise GitCommandError(execute_cmd, status, stderr)

        if extended_output:
            return (status, stdout, stderr)
        else:
            return stdout

    def log(self, branch="master",maximum=5):
        self.execute(['checkout', branch])
        logs = self.execute(['log',"--pretty='%s'","-%d"%maximum])
        result = []
        for i in logs.split('\n'):
            if i.startswith("'") and i.endswith("'"):
                i=i.lstrip("'").rstrip("'")
            result.append(i)
        return result
    
    def update(self, branch="master"):
        self.fetch()
        self.checkout(branch)

    def merge_update(self,branch="master"):
        self.fetch()
        self.checkout(branch)
        self.pull(origin="origin", branch=branch)

    def fetch(self):
        self.execute(["fetch", "-pfu"])

    def pull(self, origin=None, branch=None):
        if branch is None or origin is None:
            self.execute(["pull"])
        else:
            self.execute(["pull", origin, branch])

    def checkout(self, branch="master"):
        self.execute(["checkout", branch])

    def revision(self, branch="master"):
        return self.execute(["rev-parse",branch])


if __name__ == "__main__":
    a = Git(work_dir="/home/leaeasy/projects/git-builder/GitPython")
    for i in a.log():
        print(i)
    #a.checkout()
    #a.checkout()
    #a.pull()
    a.checkout(branch="new")
