#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

from config import BuilderConfig

from string import Template

class BuilderConfigException(Exception):
    pass

class Distribution(object):
    def __init__(self, name ):
        self.name = name

    def get_build_cmd(self, dsc):
        try:
            args = { 'n': self.name, 'd': dsc }
            t = Template(BuilderConfig().get('builder', 'build_cmd'))
            return t.safe_subsitute(**args)
        except:
            raise BuilderConfigException("Get build_cmd failed")

