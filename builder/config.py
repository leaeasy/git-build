#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

from ConfigParser import ConfigParser, NoSectionError, NoOptionError
import json
import os

class BuilderConfig(object, ConfigParser):
    config_file = "/etc/gitbuilder/gitbuilderrc"
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init(*args, **kwargs)
        return cls._instance

    def init(self, dontpaser=False):
        ConfigParser.__init__(self)
        self.add_section('build')
        self.add_section('mail')
        self.add_section('log')

        self.set('build','check_every','30')
        self.set('build','max_threads','8')
        self.set('build','max_jobs','3')
        self.set('build','kill_timeout','90')
        self.set('build','pre_build_cmd','')
        self.set('build','build_cmd','')
        self.set('build','post_build_cmd','')

        self.set('mail','from','gitbuilder@linuxdeepin.com')
        self.set('mail','mailto','status@linuxdeepin.com')
        self.set('mail','subject_prefix','[GitBuilder]')
        self.set('mail','smtp_host','localhost')
        self.set('mail','smtp_port','25')

        self.set('log','logs_dir','/var/log/gitbuilder/build_logs')
        self.set('log','time_format','%Y-%m-%d %H:%M:%S')
        self.set('log','mail_failed','0')
        self.set('log','mail_successful','0')

        if not dontpaser:
            self.reload()

    def reload(self):
        return self.read(self.config_file)

    def dump(self):
        conf = ""
        for section in self.sections():
            conf += "[" + section + "]\n"
            for key, value in self.items(section):
                conf += "%s = %s\n" % (key, value)
            conf += "\n"
        return conf

    def save(self):
        try:
            self.write(file(self.config_file,'w'))
        except Exception as error:
            print(error)
            return False
        return True

class PerConfig(object,ConfigParser):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init(*args, **kwargs)
        return cls._instance
    def init(self, config_file):
        ConfigParser.__init__(self)
        self.config_file = config_file
        self.read(self.config_file)
        self.repo = self.fetch_value('upload','host')

    def save(self):
        try:
            self.write(file(self.config_file,'w'))
        except Exception as error:
            print(error)
            return False
        return True

    @property
    def sections(self):
        return self.sections()

    def reload(self):
        self.read(self.config_file)

    def fetch_value(self, section, key, default_value=None):
        try:
            value = self.get(section, key)
        except NoSectionError:
            value = default_value
        except NoOptionError:
            value = default_value
        return value

    def add_section(self, section):
        try:
            self.add_section(section)
            self.reload()
        except:
            pass

    def add_value(self, section, key, value):
        try:
            self.set(section, key, value)
            self.reload()
        except:
            pass

class JobConfig(object):
    def __init__(self, config_file):
        self.cf = ConfigParser()
        self.cf.read(config_file)

    def fetch_value(self, name, item='default', default_value=None):
        result = {}
        if item in self.cf.sections():
            _result = self.cf.items(item)
        else:
            _result = self.cf.items('default')
        for key, value in _result:
            if key == name:
                return value
        return default_value

    def fetch_info(self, item='default'):
        result = {}
        if not item in self.cf.sections():
            return result
        for key, value in self.cf.items(item):
            result.update({key: value})
        return result

if __name__ == "__main__":
    j = JobConfig(json_file="/home/leaeasy/deepin.dsc/branchs.json")
    print(j.fetch_value(name="dbus-factory", key='pkg'))
    print(j.fetch_value(name="dbus-factory", key='base_version'))
