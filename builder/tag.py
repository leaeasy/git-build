#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import string
from ConfigParser import ConfigParser, NoSectionError, NoOptionError
import os
import re
import subprocess
from subprocess import Popen, PIPE
import time
import tempfile

BUILDER_NAME = "TagBuilder"
BUILDER_EMAIL = "tagbuilder@deepin.com"

class TagCommandError(Exception):
    def __init__(self, command, status, stderr=None):
        self.stderr = stderr
        self.status = status
        self.command = command

    def __str__(self):
        return("'%s' returned exit status %i: %s" % (' '.join(str(i) for i in self.command), self.status, self.stderr))

class Tag(object):
    def __init__(self, work_dir=None):
        self._work_dir = work_dir

    @property
    def work_dir(self):
        return self._work_dir

    def execute(self, subprocess_args=None, extended_output=None):
        if self._work_dir is None:
            cwd = os.getcwd()
        else:
            cwd = self._work_dir

        if type(subprocess_args) is not list:
            subprocess_args=subprocess_args.split()
        env = os.environ.copy()
        env["LC_MESSAGE"] = "C"
        execute_cmd = ['git']+list(subprocess_args)

        proc = Popen(execute_cmd, env=env, cwd=cwd, stdout=PIPE, stderr=PIPE)
        try:
            stdout, stderr = proc.communicate()
            status = proc.returncode
            if stdout.endswith(b'\n'):
                stdout = stdout[:-1]
            if stderr.endswith(b'\n'):
                stderr = stderr[:-1]
        finally:
            proc.stdout.close()
            proc.stderr.close()

        if status != 0:
            raise TagCommandError(execute_cmd, status, stderr)

        if extended_output:
            return (status, stdout, stderr)
        else:
            return stdout

    def tagvers(self):
        return self.execute(subprocess_args=['tag']).split()

    def commit_logs(self, source_tag, dest_tag=None):
        tags = self.tagvers()
        if dest_tag is None:
            if source_tag in tags:
                return self.execute("log --oneline %s..." % source_tag)
            else:
                return None
        else:
            if source_tag in tags and dest_tag in tags:
                return self.execute("log --oneline %s..%s" % (source_tag, dest_tag))
            elif from_tag in tags:
                return self.execute("log --oneline %s..." % source_tag)
            else:
                return None

    def lastest_tagver(self, branch=None):
        curtag = self.execute("rev-list --tags --max-count=1")
        _tagver = self.execute("describe --tags %s" % curtag)
        tagver = _tagver.replace('v','')
        # Get tag version from branch
        #    commit = self.execute("rev-parse %s" % branch)
        #    _tagver = self.execute("describe --tags --abbrev=0 %s" % commit)
        #    tagver = _tagver.replace('v','')
        return tagver, _tagver

    def pkgver(self, branch="master", fallback_version=None):
        #git describe --tags --long release/3.0
        try:
            output = self.execute(subprocess_args=("describe --tags --long %s" % branch))
            [_, rev, sha] = output.split("-")[:3]
            #ver = re.sub('^\D*', '', ver)
            ver, _ = self.lastest_tagver(branch=branch)
        except:
            if fallback_version is None:
                print("No tag found...treat version as 0.0")
                ver = "0.0"
            else:
                print("No tag found...treat version as %s" % fallback_version)
                ver = fallback_version

            rev = self.execute(subprocess_args=("rev-list --count HEAD"))
            sha = self.execute(subprocess_args=("rev-parse --short HEAD"))
        return "%s+r%s~%s" % (ver, rev, sha)

        #tagver = self.lastest_tagver(branch=branch)
        #timestamp = self.execute(subprocess_args=("log --max-count=1 --pretty=format:%%at %s" % tagver))
        #commits = self.execute(subprocess_args=(("rev-list --count HEAD --since=%s" % timestamp).split()))
        #sha = self.execute(subprocess_args=("rev-parse --short HEAD").split())
        #return "%s+r%s~g%s" % (tagver, commits, sha)

    def archive(self, source, dest, branch=None, tagver=None, force=False):
        info = {'source': source, 'dest': dest}
        if tagver is not None:
            info['tagver'] = tagver
            info['_tagver'] = tagver
        else:
            tagver, _tagver =  self.lastest_tagver(branch=branch)
            info['tagver'] = tagver
            info['_tagver'] = _tagver

        command = string.Template("git archive --prefix=${source}-${tagver}/ ${_tagver} | xz > ${dest}/${source}-${tagver}.tar.xz").substitute(info)
        print("... Run command: %s" % command)

        if not os.path.exists(dest):
            os.makedirs(dest)

        if not os.path.exists(os.path.join(dest, "%s-%s.tar.xz" % (info['source'], info['tagver']))):
            env = os.environ.copy()
            env["LC_MESSAGE"] = "C"
            if self._work_dir is None:
                cwd = os.getcwd()
            else:
                cwd = self._work_dir

            status = Popen(command, env=env, cwd=cwd, shell=True).wait()
            info['status'] = 0
            if status != 0:
                raise TagCommandError(command.split(), status)
        return info

def checkif_stable_version(version):
    if len(version.split('.')) == 1:
        return True
    elif int(re.sub('[^0-9]', '', version.split('.')[-1])) < 10:
        return True
    else:
        return False

def merge_changelog(changelog, source, version, dist, urgency="low", check_version=False, rebuild=False):
    try:
        proc = subprocess.Popen(['dpkg-parsechangelog', '-l%s' % changelog, '-SVersion'], stdout=subprocess.PIPE)
        orig_version, _ = proc.communicate()
        proc.stdout.close()
        if orig_version.endswith('\n'):
            orig_version = orig_version[:-1]

        _rebuild = False

        if version.split('-')[0] == orig_version.split('-')[0]: 
            if rebuild is True:
                if len(orig_version.split('-')) == 1:
                    version = "%s-%d" % (orig_version , 1)
                else:
                    _release = int(orig_version.split('-')[1]) + 1
                    version = "%s-%d" % (orig_version.split('-')[0], _release)
                    _rebuild = True
            else:
                raise OSError("Version: %s Already build" % version)
        if check_version:
            status = subprocess.call(['dpkg', '--compare-versions', version, 'gt', orig_version])
            if status != 0:
                raise OSError("Version lower!")

        tmpfile = tempfile.mktemp(prefix=source)
        cw = open(tmpfile, 'w')
        cw.write("%s (%s) %s; urgency=%s\n" % (source, version, dist, urgency))
        cw.write("\n")
        if _rebuild:
            cw.write("  * Rebuild Version %s \n" % version.split('-')[0])
        else:
            cw.write("  * Autobuild Tag %s \n" % version.split('-')[0])
        cw.write("\n")
        cw.write(" -- %s <%s>  %s\n" % (BUILDER_NAME, BUILDER_EMAIL, time.strftime("%a, %d %b %Y %T %z", time.localtime())))
        cw.write("\n")
        with open(changelog) as fp:
            for line in fp.readlines():
                cw.write(line)
        cw.close()
        subprocess.call(['mv', tmpfile, changelog])
        return version

    except Exception as e:
        raise(e)

class Archiver(object, ConfigParser):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init(*args, **kwargs)
        return cls._instance

    def init(self, config):
        ConfigParser.__init__(self)
        self.config = config
        self.reload()
        self.git_repo = self.fetch_value('archive', 'git_repo')
        if not os.path.exists(self.git_repo):
            os.makedirs(self.git_repo)
        self._source_repo = self.fetch_value('archive', 'source_repo')

    def reload(self):
        return self.read(self.config)

    def fetch_value(self, section, key, default_value=None):
        try:
            value = self.get(section, key)
        except (NoSectionError, NoOptionError):
            value = default_value
        return value

    def archive(self, source, tagver=None, force=False):
        repo = os.path.join(self.git_repo, source)
        try:
            if not os.path.exists(repo):
                git = self.fetch_value(source, 'git')
                if git is None:
                    raise OSError("%s is not exists, and not git found" % repo)
                status = Popen(['git', 'clone', '--bare', git, source], cwd=self.git_repo).wait()
                if status != 0:
                    raise OSError("git clone %s faild: %d" % (git, status))

            status = Popen(['git', 'fetch', '-pt'], cwd=repo).wait()
            if status != 0:
                raise OSError("git fetch %s faild: %d" % (git, status))

        except OSError as e:
            print(e)

        Repo = Tag(repo)
        branch=self.fetch_value(source,'branch',None)
        version,_ = Repo.lastest_tagver(branch=branch)
        if checkif_stable_version(version):
            dist = 'stable'
        else:
            dist = 'unstable'
        source_repo = self._source_repo.replace('{dist}', dist).replace('{source}', source)
        info = Repo.archive(source, source_repo,branch=branch,tagver=tagver, force=force)
        info['dist'] = dist
        mirror = self.fetch_value(source, 'package_host') or self.fetch_value('default', 'package_host')
        if mirror:
            info['mirror']  = mirror
        return info

    def debianize(self, info, rebuild=False):
        dsc_repo = self.fetch_value('archive', 'dsc_repo')
        print(dsc_repo)
        if not os.path.exists(dsc_repo):
            os.makedirs(dsc_repo)

        debian_repo = self.fetch_value('archive', 'debian_repo')

        source_tarball = "%s-%s.tar.xz" % (info['source'], info['tagver'])
        orig_tarball = "%s_%s.orig.tar.xz" % (info['source'], info['tagver'])

        source_tarball_path = os.path.join(info['dest'], source_tarball)
        try:
            if not os.path.exists(source_tarball_path):
                raise OSError("No source found!")

            tempdir = tempfile.mkdtemp(prefix=info['source'])
            subprocess.call(['cp', '-ar', source_tarball_path, orig_tarball], cwd=tempdir)
            subprocess.call(['tar', '-xf', orig_tarball], cwd=tempdir)
            temp_source_dir = info['source'] + '-' + info['tagver']
            if os.path.exists(os.path.join(debian_repo, info['source'], 'debian')):
                changelog = os.path.join(debian_repo, info['source'], 'debian', 'changelog')
                build_version = merge_changelog(changelog, info['source'], info['tagver']+'-1', info['dist'], rebuild=rebuild)
                subprocess.call(['cp', '-ar', os.path.join(debian_repo, info['source'], 'debian'), temp_source_dir], cwd=tempdir)
            else:
                build_version = merge_changelog(os.path.join(tempdir, temp_source_dir, 'debian', 'changelog'), info['source'], info['tagver']+'-1', info['dist'], rebuild=rebuild)

            _format = os.path.join(tempdir, temp_source_dir, 'debian', 'source', 'format')
            if os.path.exists(_format):
                with open(_format, 'w') as fp:
                    fp.write('3.0 (quilt)')
            subprocess.call(['dpkg-source', '--include-removal', '-b', temp_source_dir], cwd=tempdir)
            subprocess.call("dpkg-genchanges -S -sa > ../%s_%s_source.changes" %(info['source'], build_version), shell=True, cwd=os.path.join(tempdir, temp_source_dir))
            dsc = os.path.join(tempdir, info['source'] + '_' + build_version + '.dsc')
            if not os.path.exists(dsc):
                raise OSError("No dsc found")

            for i in os.listdir(tempdir):
                if os.path.isfile(os.path.join(tempdir, i)):
                    subprocess.call(['cp', '-av', os.path.join(tempdir, i), dsc_repo])
            return build_version
        except OSError as e:
            raise(e)
        finally:
            subprocess.call(['rm', '-rf', tempdir])

if __name__ == "__main__":
    A = Archiver()
    info = A.archive('deepin-mutter')
    A.debianize(info)
