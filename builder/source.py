#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import os
from debian.changelog import Changelog
import subprocess
import glob

class GenerateSourceException(Exception):
    pass

def deco_chdir(func):
    def _deco(*args, **kwargs):
        owd = os.getcwd()
        try:
            os.chdir(args[0])
            ret = func(*args, **kwargs)
        finally:
            os.chdir(owd)
        return ret
    return  _deco


def get_parent_dir(path, level = 1):
    parent_dir = os.path.realpath(path)
    while level > 0:
        parent_dir = os.path.dirname(parent_dir)
        level -= 1
    return parent_dir
        

@deco_chdir
def generate_dsc(path):
    try:
        format_file = open(os.path.join(path,'debian','source','format'))
        for line in format_file.readlines():
            if 'native' in line:
                isNative = True
            else:
                isNative = False
    except:
        isNative = True

    changelog_file = os.path.join(path,'debian','changelog')
    if not os.access(changelog_file, os.F_OK):
        raise GenerateSourceException("No changelog file exist")
    changelog = Changelog(open(changelog_file))
    version = str(changelog.version)
    package = str(changelog.package)

    parent_dir = get_parent_dir(path)
    
    if ':' in version:
        version = version.split(':')[1]

    if not isNative:
        orig_version = version.split('-')[0]
        print("Generate orig source tar file...")
        cmd = "tar -caf ../%s_%s.orig.tar.xz * --exclude=debian" % (package, orig_version)
        if subprocess.Popen(cmd, shell=True).wait() !=0:
            raise GenerateSourceException("Generate orig source tar file Failed\nCommand: %s" % cmd)

    #cmd = 'yes | debuild --no-lintian -S -sa -I.git -I.bzr -I.pc'
    cmd = 'dpkg-source -b .'
    if subprocess.Popen(cmd, shell=True).wait() != 0:
        raise GenerateSourceException("Generate orig source tar file Failed\nCommand: %s" % cmd)

    dscfile = "%s_%s.dsc" % (package, version)
    dscpath = os.path.join(parent_dir, dscfile)
    if os.path.exists(dscpath):
        os.chdir(parent_dir)
        return parent_dir,glob.glob("%s_*" % package) 
    else:
        raise GenerateSourceException("Generate dsc file failed! No dsc generate~")
