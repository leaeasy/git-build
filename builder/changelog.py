#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import os
import subprocess
import email

class ChangeLog(object):
    def __init__(self, contents=None, filename=None):
        self._contents = ''
        self._cp = None
        self._filename = filename

        if (not filename and not contents) or ( filename and contents):
            raise Exception("Either filename or content must be passed")

        if filename and not os.access(filename, os.F_OK):
            raise  Exception("Changelog %s not found" % (filename,))

        if contents:
            self._contents = contents[:]
        else:
            self._read()
        self._parse()

    def _parse(self):
        cmd = subprocess.Popen(['dpkg-parsechangelog', '-l-'],
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
        (output, error) = cmd.communicate(self._contents)
        if cmd.returncode:
            raise Exception("Failed to parse changelog." "dpkg-parsechangelog said:\n %s" % (error,))

        cp = email.message_from_string(output)
        try:
            if ':' in cp['Version']:
                cp['Epoch'], cp['NoEpoch-Version'] = cp['Version'].split(':',1)
            else:
                cp['NoEpoch-Version'] = cp['Version']
            if '-' in cp['NoEpoch-Version']:
                cp['Upstream-Version'], cp['Debian-Version'] = cp['NoEpoch-Version'].rsplit('-',1)
            else:
                cp['Debian-Version'] = cp['NoEpoch-Version']
        except TypeError:
            raise Exception("Paser changelog Version error: %s" % output.split('\n')[0])

        self._cp = cp

    def _read(self):
        with open(self.filename) as f:
            self._contents = f.read()

    def __getitem__(self, item):
        return self._cp[item]

    def __setitem__(self, item, value):
        self._cp[item] = value

    @property
    def filename(self):
        return self._filename

    @property
    def name(self):
        return self._cp['Source']

    @property
    def version(self):
        return self._cp['Version']

    @property
    def upstream_version(self):
        return self._cp['Upstream-Version']

    @property
    def debian_version(self):
        return self._cp['Debian-Version']

    @property
    def epoch(self):
        return self._cp['Epoch']

    @property
    def noepoch(self):
        return self._cp['NoEpoch-Version']

    def has_epoch(self):
        return self._cp.has_key('Epoch')

    @property
    def author(self):
        return email.Utils.parseaddr(self._cp['Maintainer'])[0]

    @property
    def email(self):
        return email.Utils.parseaddr(self._cp['Maintainer'])[1]

    @property
    def date(self):
        return self._cp['Date']
