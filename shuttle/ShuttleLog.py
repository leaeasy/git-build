#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import logging
import sqlobject

from ShuttleConfig import ShuttleConfig

class Log(sqlobject.SQLObject):
    job = sqlobject.ForeignKey('Job', cascade=True)
    text = sqlobject.BLOBCol(default="No build log available" ,length=2**24)

class ShuttleLog(object):

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init()
        return cls._instance

    def init(self):
        cfg = ShuttleConfig()
        logging.basicConfig(level = logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            filename=cfg.get('log', 'file'),
                            datefmt=cfg.get('log', 'time_format'),
                            filemode='a')
    @classmethod
    def info(self, str):
        logging.info(str)

    @classmethod
    def warn(self,str):
        logging.warning(str)

    @classmethod
    def error(self, str):
        logging.error(str)

