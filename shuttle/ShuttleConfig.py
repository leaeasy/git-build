#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
# shuttle - Deepin packages build tool

import ConfigParser
import os

class ShuttleConfig(object, ConfigParser.ConfigParser):
    config_file = "/etc/shuttle/shuttlerc"
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init(*args, **kwargs)
        return cls._instance

    def init(self, dontparse=False):
        ConfigParser.ConfigParser.__init__(self)
        self.add_section('build')
        self.add_section('mail')
        self.add_section('http')
        self.add_section('log')

        self.set('build','check_every', '300')
        self.set('build', 'max_threads', '2')
        self.set('build', 'max_jobs', '5')
        self.set('build', 'kill_timeout', '90')
        self.set('build', 'source_cmd', 'apt-get -q --download-only -t ${d} source ${p}=${v}')
        self.set('build', 'build_cmd', 'pbuilder build --basetgz /var/cache/pbuilder/${d}-${a}.tgz ${p}_${v}.dsc')
        self.set('build', 'post_build_cmd', '')
        self.set('build', 'upload_package_cmd', '')
        self.set('build', 'dists', 'squeeze wheezy sid')
        self.set('build', 'work_dir', '/var/cache/shuttle/build')
        self.set('build', 'database_uri', 'sqlite:///var/lib/shuttle/shuttle.db')
        self.set('build', 'build_more_recent', '1')
        self.set('build', 'more_archs', 'any')
        self.set('build', 'no_system_arch', '1')

        self.set('mail', 'from', 'shuttle@localhost')
        self.set('mail', 'mailto', 'shuttle@localhost')
        self.set('mail', 'subject_prefix', '[shuttle]')
        self.set('mail', 'smtp_host', 'localhost')
        self.set('mail', 'smtp_port', '25')

        self.set('http', 'port', '9998')
        self.set('http', 'ip', '0.0.0.0')
        # This is dedicated to MadCoder
        self.set('http', 'log_lines_nb', '30')
        self.set('http', 'templates_dir', '/usr/share/shuttle/templates')
        self.set('http', 'cache', '1')
        self.set('http', 'logfile', '/var/log/shuttle/httpd.log')

        self.set('log', 'file', "/var/log/shuttle/shuttle.log")
        self.set('log', 'time_format', "%Y-%m-%d %H:%M:%S")
        self.set('log', 'logs_dir', "/var/log/shuttle/build_logs")
        self.set('log', 'mail_failed', '1')
        self.set('log', 'mail_successful', '0')

        if not dontparse:
            self.reload()

        self.arch = []
        if self.getint('build','no_system_arch') == 0:
            parch = os.popen("dpkg --print-architecture")
            self.arch.append(parch.readline().strip())
            parch.close()

        for a in self.get('build','more_archs').split(' '):
            self.arch.append(a)

    def reload(self):
        """ Reload configuration file """
        return self.read(self.config_file)

    def dump(self):
        """ Dump running configuration """
        conf = ""
        for section in self.sections():
            conf += "[" + section + "]\n"
            for item, value in self.items(section):
                conf += "%s = %s\n" % (item, value)
            conf += "\n"
        return conf

    def save(self):
        """ Save configuration file """
        try:
            self.write(file(self.config_file, 'w'))
        except Exception as error:
            print(error)
            return False
        return True
