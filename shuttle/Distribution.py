#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

from ShuttleConfig import ShuttleConfig
from ShuttleLog import ShuttleLog
from string import Template

class Distribution(object):
    """ Class implement a Debian/Ubuntu distribution

    Substitutions are done in the command strings:
        $d => The distro name
        $a => the target architecture
        $p => the package name
        $v => them package version
    """
    def __init__(self, name, arch):
        self.name = name
        self.arch = arch

    def get_source_cmd(self, package, mirror=None):
        """ Return commnad used for grabing source """
        try:
            args = { 'd':self.name, 'a':self.arch, 'v':package.version, 'p':package.name, 'm': mirror}
            t = Template(ShuttleConfig().get('build','source_cmd'))
            return t.safe_substitute(**args)
        except TypeError as error:
            ShuttleLog.error("get_source_cmd has invalid format: %s" % error)
            return None

    def get_build_cmd(self, package, mirror=None):
        """ Return commnad used for building source """
        try:
            # Strip version like 1:2.01
            if ":" in package.version:
                index = package.version.index(":")
                args = { 'd':self.name, 'a':self.arch, 'v':package.version[index+1:], 'p': package.name, 'm': mirror}
            else:
                args = { 'd':self.name, 'a':self.arch, 'v':package.version, 'p':package.name, 'm': mirror }
            t = Template(ShuttleConfig().get('build', 'build_cmd'))
            return t.safe_substitute(**args)

        except TypeError as error:
            ShuttleLog.error("get_build_cmd has invalid format: %s" % error)
            return None

    def get_post_build_cmd(self, package, mirror=None):
        cmd = ShuttleConfig().get('build', 'post_build_cmd')
        if cmd == '':
            return None
        try:
            args = { 'd':self.name, 'a':self.arch, 'v': package.version, 'p': package.name , 'm': mirror}
            t = Template(cmd)
            return t.safe_substitute(**args)
        except TypeError as error:
            ShuttleLog.error("post_build_cmd has invalid format: %s" % error)
            return None
        
    def get_upload_package_cmd(self, package, mirror=None):
        cmd = ShuttleConfig().get('build', 'upload_package_cmd')
        if mirror is None or cmd == '':
            return None
        try:
            args = { 'd':self.name, 'a':self.arch, 'v': package.version, 'p': package.name , 'm': mirror}
            t = Template(cmd)
            return t.safe_substitute(**args)
        except TypeError as error:
            ShuttleLog.error("upload_package_cmd has invalid format: %s" % error)
            return None
