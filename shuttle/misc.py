#!/usr/bin/env python2

import types

class Enumeration: 
    """Simple enumeration class with reverse lookup""" 

    def __init__(self, enumlist): 
        self.lookup = { } 
        self.reverse_lookup = { } 
        val = 0 
        for elem in enumlist: 
            if type(elem) == types.TupleType: 
                elem, val = elem 
            if type(elem) != types.StringType: 
                raise ValueError("enum name is not a string: " + elem)
            if type(val) != types.IntType: 
                raise ValueError("enum value is not an integer: " + val)
            if self.lookup.has_key(elem): 
                raise ValueError("enum name is not unique: " + elem)
            if val in self.lookup.values(): 
                raise ValueError("enum value is not unique for " + val)
            self.lookup[elem] = val 
            self.reverse_lookup[val] = elem 
            val += 1 

    def __getattr__(self, attr): 
        if not self.lookup.has_key(attr): 
            raise AttributeError 
        return self.lookup[attr] 

    def whatis(self, value): 
        """Return element name for a value""" 

        return self.reverse_lookup[value]

class Dists(object):
    _instance = None
    dists = []

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
        return cls._instance
    
    def add_dist(self, dist):
        self.dists.append(dist)

    def get_dist(self, name, arch):
        for d in self.dists:
            if d.name == name and d.arch == arch:
                return d
        return None
