#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

import sqlobject

import apt_pkg
apt_pkg.init_system()

class Package(sqlobject.SQLObject):
    name = sqlobject.StringCol()
    version = sqlobject.StringCol(default=None)
    priority = sqlobject.StringCol(default=None)

    @staticmethod
    def version_compare(a,b):
        try:
            return apt_pkg.version_compare(a.version, b.version)
        except:
            # FIXME: Maybe a.version or b.version is illegal
            return False

