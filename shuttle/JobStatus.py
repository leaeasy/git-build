#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

from misc import Enumeration

JobStatus = Enumeration([ ("UNKNOWN", 0), 
                          ("WAIT", 100), 
                          ("WAIT_LOCKED", 150),
                          ("BUILDING", 200), 
                          ("SOURCE_FAILED", 250), 
                          ("BUILD_FAILED", 300), 
                          ("POST_BUILD_FAILED", 350), 
                          ("UPLOAD_PACKAGE_FAILED",500),
                          ("CANCELED", 800), 
                          ("GIVEUP", 850),
                          ("FAILED", 900),
                          ("BUILD_OK", 1000) ])

FailedStatus = (JobStatus.SOURCE_FAILED,
                JobStatus.BUILD_FAILED,
                JobStatus.POST_BUILD_FAILED,
                JobStatus.UPLOAD_PACKAGE_FAILED,
                JobStatus.FAILED)
