import urllib2
import json

class BearyChat(object):
    def __init__(self, url):
        self.url = url
        self.header = {'Content-Type': 'application/json; charset=UTF-8'}

    def message(self, message_text, message_channel=[],timeout=15):
        srcdata = dict()
        srcdata["text"] = message_text
        if message_channel:
            srcdata["channel"] = ",".join(message_channel)

        data = json.dumps(srcdata)
        request = urllib2.Request(self.url, data, self.header)
        opener = urllib2.build_opener()
        return opener.open(request,None, timeout).read()
