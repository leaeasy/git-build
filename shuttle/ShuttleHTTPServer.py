#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

from ShuttleConfig import ShuttleConfig
from Shuttle import Shuttle
from Package import Package
from Job import Job
from JobStatus import JobStatus
from JobStatus import FailedStatus

import tempfile, socket, sqlobject
import web
#import gdchart

import sys
import os
sys.path.insert(0,'..')
reload(sys)
import GitBuildSource
from GitBuildSource import GitBuilder
from builder.git import Git

render = web.template.render(ShuttleConfig().get('http', 'templates_dir'), cache=ShuttleConfig().getboolean('http', 'cache'))
debian_repo = ShuttleConfig().get('build', 'debian_repo')

class RequestIndex:

    def GET(self):
        return render.index(hostname=socket.gethostname(), archs=ShuttleConfig().arch, dists=ShuttleConfig().get('build', 'dists').split(' '))

class RequestPackage:

    def GET(self, name=None, version=None):
        jobs = []

        if version:
            pkg = Package.selectBy(name=name, version=version)[0]
            title = "%s %s" % (name, version)
            package = "%s/%s" % (name, version)
            jobs.extend(Job.selectBy(package=pkg))
        else:
            pkgs = Package.selectBy(name=name).orderBy("-id")[:100]
            title = package = name
            for pkg in pkgs:
                jobs.extend(Job.selectBy(package=pkg))
        
        return render.base(page=render.tab(jobs=jobs), \
                hostname=socket.gethostname(), \
                title=title, \
                package=package, \
                archs=ShuttleConfig().arch, \
                dists=ShuttleConfig().get('build', 'dists').split(' '))

class RequestArch:

    def GET(self, dist, arch=None):
        jobs = []
        jobs.extend(Job.select(sqlobject.AND(Job.q.arch == arch, Job.q.dist == dist),
            orderBy=sqlobject.DESC(Job.q.creation_date))[:300])
        return render.base(page=render.tab(jobs=jobs), \
                arch=arch, \
                dist=dist, \
                title="%s/%s" % (dist, arch), \
                hostname=socket.gethostname(), \
                archs=ShuttleConfig().arch, \
                dists=ShuttleConfig().get('build', 'dists').split(' '))

class RequestJob:

    def GET(self, jobid=None):
        job = Job.selectBy(id=jobid)[0]

        try:
            with open(job.logfile, "r") as build_logfile:
                build_log = build_logfile.read()
        except IOError as error:
            build_log = job.log.text

        return render.base(page=render.job(job=job, build_log=build_log), \
                hostname=socket.gethostname(), \
                title="job %s" % job.id, \
                archs=ShuttleConfig().arch, \
                dists=ShuttleConfig().get('build', 'dists').split(' '))

class RequestGraph:

    GET = web.autodelegate("GET_")

    def graph_init(self):
	return "dummy"
        web.header("Content-Type","image/png") 
        graph = gdchart.Bar3D()
        graph.width = 300
        graph.height = 300
        graph.ytitle = "Jobs"
        graph.xtitle = "Build status"
        graph.ext_color = [ "yellow", "orange", "red", "green"]
        graph.bg_color = "white"
        graph.setLabels(["WAIT", "BUILDING", "FAILED", "OK"])

        return graph

    def compute_stats(self, jobs):
        jw = 0
        jb = 0
        jf = 0
        jo = 0
        for job in jobs:
            if job.status == JobStatus.WAIT or \
               job.status == JobStatus.WAIT_LOCKED:
                jw += 1
            elif job.status == JobStatus.BUILDING:
                jb += 1
            elif job.status in FailedStatus:
                jf += 1
            elif job.status == JobStatus.BUILD_OK:
                jo += 1

        return (jw, jb, jf, jo)

    def GET_buildstats(self, distarch=None):
        graph = self.graph_init()
        if distarch == "/":
            graph.title = "Build status"
            jobs = Job.selectBy()
        else:
            dindex = distarch.rindex("/")
            graph.title = "Build status for %s" % distarch[1:]
            jobs = Job.selectBy(arch=distarch[dindex+1:], dist=distarch[1:dindex])

        graph.setData(self.compute_stats(jobs))
        tmp = tempfile.TemporaryFile()
        graph.draw(tmp)
        tmp.seek(0)
        return tmp.read()

    def GET_package(self, package=None):
        graph = self.graph_init()
        if package == "/":
            graph.title = "Build status"
            jobs = Job.selectBy()
        else:
            dindex = package.rindex("/")
            graph.title = "Build status for %s" % package[1:]
            pkg = Package.selectBy(version=package[dindex+1:], name=package[1:dindex])[0]
            jobs = Job.selectBy(package=pkg)

        graph.setData(self.compute_stats(jobs))
        tmp = tempfile.TemporaryFile()
        graph.draw(tmp)
        tmp.seek(0)
        return tmp.read()

class BuildPackage:

    def GET(self, package=None, branch=None):
        if package is None or branch is None:
            return {"status": 1, "info": "package or branch blank"}

        _repo = Git(work_dir=debian_repo)
        _repo.update(branch='origin/'+branch)
        _conf_file = os.path.join(debian_repo, 'builder.conf')
        if not os.path.exists(_conf_file):
            return {"status": 1, "info": "builder.conf not exists"}
        
        _p = GitBuilder(_conf_file, package, debian_branch=branch)
        try:
            _p.prepare_source()
            _p.merge_changelog()
            _p.generate_dsc()
            mirror, cp = _p.upload_source()
            Shuttle().add_job(name=cp['Source'], version=cp['MangledVersion'], priority=cp['Urgency'], dist=cp['Distribution'], mirror=mirror)
        except Exception as e:
            print(e)
        finally:
            _p.clean_workdir()

        if cp:
            return {"status": 0, "pkg":cp['Source'], "version": cp['MangledVersion'], "priority": cp['Urgency'], "dist": cp['Distribution'], "mirror": mirror}


class ShuttleHTTPServer:
    """Main HTTP server"""

    urls = (
            '/', 'RequestIndex',
            '/dist/(.*)/arch/(.*)', 'RequestArch',
            '/job/(.*)', 'RequestJob',
            '/log/(.*)', 'RequestLog',
            '/package/(.*)/(.*)', 'RequestPackage',
            '/package/(.*)', 'RequestPackage',
            '/graph/(.*)', 'RequestGraph',
            '/build/package=(.*)&branch=(.*)', 'BuildPackage',
            )

    def __init__(self):
        Shuttle()

    def start(self):

        """Run main HTTP server thread"""
        if debian_repo is None:
            raise Exception("shuttle config debian_repo should not be blank.")

        web.webapi.internalerror = web.debugerror

        import sys
        sys.argv.append(ShuttleConfig().get('http', 'ip') + ":" + ShuttleConfig().get('http', 'port'))
        app = web.application(self.urls, globals())
        app.run()

