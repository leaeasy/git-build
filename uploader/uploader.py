#!/usr/bin/env python2
#-*- coding:utf-8 -*-

from subprocess import Popen, PIPE
from error import UploaderFailedError
CONFIG_TEMPLATE = '''
[DEFAULT]
allow_dcut             = 1
hash                   = md5
allow_unsigned_uploads = 1
run_lintian            = 0
run_dinstall           = 0
check_version          = 0
scp_compress           = 0
post_upload_command    =
pre_upload_command     =
allowd_distributions   = %(name)s
progress_indicator     = 2
default_host_main      = %(name)s

[%(name)s]
login                  = %(users)s
fqdn                   = %(host)s
method                 = scp
incoming               = %(path)s
'''
class Dput():
    def __init__(self,name,key,config=None):
        self.name = name
        self.key = key
        self.config = config

    def sign(self, changes, stdout=PIPE, stderr=PIPE):
        cmd = 'debsign -k %s %s' % (self.key,changes)
        p = Popen(cmd.split()).wait()
        if p != 0:
            raise UploaderFailedError(changes, p)

    def upload(self, changes, stdout=PIPE, stderr=PIPE, delete_after_success=True):
        if self.config is None:
            cmd = 'dput %s %s' % (self.name, changes)
        else:
            cmd = 'dput -c %s %s %s' % (self.config, self.name, changes)
        p = Popen(cmd.split()).wait()
        if p != 0:
            raise UploaderFailedError(changes, p)
        else:
            if delete_after_success:
                import os
                #fix me asap
                #upload_file = changes.rstrip('changes') + self.name + '.upload'
                package_info_list = changes.split('_')
                package_wild = package_info_list[0]+'_'+package_info_list[1]+'*'
                os.system('rm -rf %s' % package_wild)
