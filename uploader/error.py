#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

class BuilderException(StandardError):
    pass

class UploaderFailedError(BuilderException):
    def __init__(self, changes, code, log=None):
        self.changes = changes
        self.code = code
        self.log = log
    def __str__(self):
        return "Upload failed: %s  (%s)" % (self.changes, self.code)

